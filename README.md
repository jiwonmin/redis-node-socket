### Running the frontend

```
npm install
npm run serve
npm run serve -- --host 0.0.0.0 --port 80 --public example.com
              OR setting vue.config.js
```

### Running the server

```
cd server
npm install
npm start
```

### Use nohup

```
nohup serve -p 80 dist >> nohup.log 2>&1 &
nohup npm start >> nohup.log 2>&1 &

ps -ef | grep {COMMAND}
kill -9 {PID}

서버 실행 후 PID 기록
```

### Redis GUI tool

- [Medis](https://github.com/luin/medis)