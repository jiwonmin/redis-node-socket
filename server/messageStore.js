// 세션 유지 시간 (초)
const CONVERSATION_TTL = (24 * 60 * 60) * 8; // 8일

class RedisMessageStore {
  constructor(redisClient) {
    this.redisClient = redisClient;
  }

  // 메세지 리스트 저장
  saveMessage(message) {
    const value = JSON.stringify(message);
    this.redisClient
      .multi()
      .rpush(`messages:${message.from}`, value)
      .rpush(`messages:${message.to}`, value)
      .expire(`messages:${message.from}`, CONVERSATION_TTL)
      .expire(`messages:${message.to}`, CONVERSATION_TTL)
      .exec();
  }

  // 유저ID 값으로 메세지 찾기
  findMessagesForUser(userID) {
    return this.redisClient
    .lrange(`messages:${userID}`, 0, -1)
    .then((results) => {
      return results.map((result) => JSON.parse(result));
    });
  }

  // 메세지 리스트 길이 반환
  getListSize(userID) {
    return this.redisClient.llen(`messages:${userID}`);
  }

  // 메세지 삭제
  removeMessage(userID) {
    this.redisClient.del(`messages:${userID}`);
    return true;
  }

  // 삭제 된 유저와 관련된 모든 메세지 삭제
  removeGarbageMessage(sessionUserID, deletedUserID) {
    this.redisClient
    .lrange(`messages:${sessionUserID}`, 0, -1)
    .then((results) => {
      for (let i = 0; i < results.length; i ++) {
        let jsonResults = JSON.parse(results[i]);
        if (jsonResults.from === deletedUserID || jsonResults.to === deletedUserID) {
          this.redisClient.lrem(`messages:${sessionUserID}`, 0, results[i]);
        }
      }
    });

    this.redisClient
    .lrange(`messages:all user`, 0, -1)
    .then((results) => {
      for (let i = 0; i < results.length; i ++) {
        let jsonResults = JSON.parse(results[i]);
        if (jsonResults.from === deletedUserID) {
          this.redisClient.lrem(`messages:all user`, 0, results[i]);
        }
      }
    });
  }
}

module.exports = {
  RedisMessageStore
};
