const httpServer = require("http").createServer();
const Redis = require("ioredis");
const redisClient = new Redis();
const io = require("socket.io")(httpServer, {
  cors: {
    origin: "*", // CORS 정책 적용, 모든 IP에서 접근 허용
  },
  adapter: require("socket.io-redis")({
    pubClient: redisClient,
    subClient: redisClient.duplicate(),
  }),
});

const { setupWorker } = require("@socket.io/sticky");
const crypto = require("crypto");
const randomId = () => crypto.randomBytes(8).toString("hex");

// 레디스, 세션 관련 쿼리
const { RedisSessionStore } = require("./sessionStore");
const sessionStore = new RedisSessionStore(redisClient);

// 레디스, 메세지 관련 쿼리
const { RedisMessageStore } = require("./messageStore");
const messageStore = new RedisMessageStore(redisClient);

io.use(async (socket, next) => {
  // 연결 시도 시, 세션ID를 넘긴 경우
  const sessionID = socket.handshake.auth.sessionID;
  if (sessionID) {
    // 세션 조회 후 연결 처리
    const session = await sessionStore.findSession(sessionID);
    if (session) {
      socket.sessionID  = sessionID;
      socket.userID     = session.userID;
      socket.username   = session.username;
      socket.password   = session.password;
      socket.newPrivate =  session.newPrivate;
      socket.newPublic  = session.newPublic;
      socket.watching   = session.watching;
      return next();
    }
  }

  // username 값 확인
  const username = socket.handshake.auth.username;
  if(username && (username.match(/\s/g) || username.length <= 2)) {
    return next(new Error("not allow spacebar"));
  }

  // password 값 확인
  const password = socket.handshake.auth.password;
  if(password && (password.match(/\s/g) || password.length <= 2)) {
    return next(new Error("not allow spacebar"));
  }

  // username 없는 경우
  if (!username) {
    return next(new Error("invalid username"));
  }

  // password 없는 경우
  if (!password) {
    return next(new Error("invalid password"));
  }

  // 비밀번호 암호화
  const cryptoPassword = 
          Buffer.from(
            crypto
              .createHash('sha512')
              .update(password)
              .digest('binary'),
            'binary'
          ).toString('base64').slice(0,60)
  ;

  // 입력한 이름/비밀번호가 존재하는 경우
  const isExistsUser = await sessionStore.findUserByLoginInfo(username, cryptoPassword);

  // 비밀번호 불일치, 이름만 일치하는 경우
  const isExistsUsername = await sessionStore.findUsernameByInputName(username);

  if (isExistsUser) {
    const tempSID     = await sessionStore.findSessionIDByUsername(username);
    const tempUID     = await sessionStore.findUserIDByUsername(username);
    socket.sessionID  = tempSID.replace('session:', '');
    socket.userID     = tempUID;
  } else if (isExistsUsername) {
    return next(new Error("password failed"));
  } else {
    socket.sessionID  = randomId();
    socket.userID     = randomId();
  }

  socket.username     = username;
  socket.password     = cryptoPassword;
  socket.newPrivate   = false;
  socket.newPublic    = false;
  socket.watching     = false;
  next();
});

io.on("connection", async (socket) => {
  sessionStore.saveSession(socket.sessionID, {
    userID    : socket.userID,
    username  : socket.username,
    password  : socket.password,
    connected : true,
    newPrivate: socket.newPrivate,
    newPublic : socket.newPublic,
    watching  : socket.watching,
  });

  // 세션 정보 클라이언트에 전달
  socket.emit("session", {
    sessionID : socket.sessionID,
    userID    : socket.userID,
  });

  // 1:1 방 생성
  socket.join(socket.userID);

  
  const users = [];
  // 접속한 유저 관련 메세지, 모든 세션 정보 변수 저장
  const [messages, sessions] = await Promise.all([
    messageStore.findMessagesForUser(socket.userID),
    sessionStore.findAllSessions(),
  ]);

  // 유저별 메세지 저장 변수
  const messagesPerUser = new Map();
  messages.forEach((message) => {
    const { from, to } = message;
    const otherUser = socket.userID === from ? to : from;

    // 보낸유저, 받는유저에 따라 맵 형식으로 데이터 저장
    if (messagesPerUser.has(otherUser)) {
      messagesPerUser.get(otherUser).push(message);
    } else {
      messagesPerUser.set(otherUser, [message]);
    }
  });

  sessions.forEach((session) => {
    // 유저별로 메세지 별도 저장
    users.push({
      userID    : session.userID,
      username  : session.username,
      password  : session.password,
      connected : session.connected,
      newPrivate: session.newPrivate,
      newPublic : session.newPublic,
      watching  : session.watching,
      messages  : messagesPerUser.get(session.userID) || [],
    });
  });

  // 유저별 메세지 포함해서 유저 정보 클라이언트에 전달
  socket.emit("users", users);

  // 접속한 유저수 계산
  socket.emit("online count", users);
  socket.broadcast.emit("online count", users);

  // Public Room 모든 메세지 클라이언트에 전달
  const publicMessages = await messageStore.findMessagesForUser("all user");
  socket.emit("all public messages", publicMessages);


  // 로그인한 현재 유저, 다른 유저에게 공지
  socket.broadcast.emit("user connected", {
    userID: socket.userID,
    username: socket.username,
    password: socket.password,
    connected: true,
    newPrivate: socket.newPrivate,
    newPublic: socket.newPublic,
    watching: socket.watching,
    messages: [],
  });

  // 유저 제거
  socket.on("remove user", async (userID, index) => {
    const foundSessionID    = await sessionStore.findSessionIDByUserID(userID); // 선택된 유저 세션ID
    const isUserRemoved     = await sessionStore.removeSession(userID);         // 제거 후 완료 여부
    const isMessageRemoved  = messageStore.removeMessage(userID);               // 개인 메세지까지 모두 제거 후 완료 여부

    // 다른 세션에 저장되어있는 메세지까지 모두 제거
    sessions.forEach(session => {
      messageStore.removeGarbageMessage(session.userID, userID);
    });

    if (isUserRemoved === true && isMessageRemoved === true) {
      socket.emit("user removed", index, foundSessionID);           // 클라이언트의 유저 목록에서 해당 유저 제거
      socket.broadcast.emit("user removed", index, foundSessionID); // 클라이언트의 유저 목록에서 해당 유저 제거 및 localStorage에서 세션 정보 삭제
    }
  });

  // 1:1 메세지 전송
  socket.on("private message", async ({ content, to }) => {
    // --------------------------------------------------------
    // ------------------- 유저 정보 업데이트 -------------------
    const sessionID   = await sessionStore.findSessionIDByUserID(to); // 받는 유저의 세션ID
    const session     = await sessionStore.findSession(sessionID);    // 받는 유저의 세션정보
    let userMessages  = await messageStore.findMessagesForUser(to);   // 받는 유저의 메세지
    let tempPrivate   = JSON.parse(session.newPrivate);               // 기존에 가지고 있는 새 메세지 정보
    let newPrivate    = [];

    // 기존 새 메세지(newPrivate) 정보에 있는 유저인지 조회
    let existsUserID = false;
    for (let i = 0; i < tempPrivate.length; i++) {
      if (tempPrivate[i].userID === socket.userID) {
        existsUserID = true;
      }

      // 기존 정보를 업데이트하기 위해 변수에 저장
      newPrivate.push(tempPrivate[i]);
    }

    // 기존 새 메세지(newPrivate) 정보에 없는 경우
    if (!existsUserID) {
      // 메세지 개수로 마지막 인덱스 값 확인
      let lastIndex = 0;
      userMessages.forEach((value) => {
        if (value.from === socket.userID || value.to === socket.userID) {
          lastIndex++;
        }
      });

      // 새 메세지 정보 입력
      newPrivate.push({
        userID: socket.userID,
        lastIndex: lastIndex
      });
    }

    // 메세지를 받는 유저가 채팅창을 보고있으면 새 메세지 정보 false 입력
    if (session.watching === socket.userID) {
      newPrivate = false;
    } else {
      newPrivate = JSON.stringify(newPrivate);
    }

    // 새 메세지 정보 포함해서 세션 정보 업데이트
    sessionStore.saveSession(sessionID, {
      userID    : session.userID,
      username  : session.username,
      password  : session.password,
      connected : session.connected,
      newPrivate: newPrivate,
      newPublic : session.newPublic,
      watching  : session.watching,
    });
    // -------------------- /유저 정보 업데이트 --------------------
    // -----------------------------------------------------------

    // 메세지 저장
    const message = {content, from: socket.userID, to};
    socket.to(to).to(socket.userID).emit("private message", message);
    messageStore.saveMessage(message);

    // 새 메세지 여부 표시
    socket.emit("check newPrivate", JSON.parse(newPrivate));
    socket.to(to).emit("check newPrivate", JSON.parse(newPrivate));
  });

  // 1:1 새 메세지 읽음 처리
  socket.on("check newPrivate", async (userID) => {
    const sessionID   = await sessionStore.findSessionIDByUserID(socket.userID); // 현재 접속한 유저의 세션ID
    const session     = await sessionStore.findSession(sessionID);               // 현재 접속한 유저의 세션정보
    let newPrivate    = JSON.parse(session.newPrivate);                          // 현재 접속한 유저의 새 메세지 정보

    // 새 메세지 정보가 있는 경우
    if (newPrivate) {
      for (let i = 0; i < newPrivate.length; i++) {
        // 새 메세지 정보에 선택한 userID가 있다면 삭제 처리(읽음 처리)
        if (newPrivate[i].userID === userID) {
          delete newPrivate[i];
        }
      }

      // 삭제 후 빈 배열의 경우 false 입력
      newPrivate = newPrivate.filter(Boolean);
      newPrivate = (newPrivate.length === 0) ? false : JSON.stringify(newPrivate);
    }

    // 현재 접속한 유저의 새 메세지 업데이트
    sessionStore.saveSession(sessionID, {
      userID    : session.userID,
      username  : session.username,
      password  : session.password,
      connected : session.connected,
      newPrivate: newPrivate,
      newPublic : session.newPublic,
      watching  : session.watching,
    });

    // 선택한 유저에게 새 메세지 읽음 표시 전달
    const selectedSID      = await sessionStore.findSessionIDByUserID(userID); // 선택한 유저의 세션ID
    const selectedSession  = await sessionStore .findSession(selectedSID);     // 선택한 유저의 세션정보
    let selectedNewPrivate = JSON.parse(selectedSession.newPrivate);           // 선택한 유저의 새 메세지 정보

    // 새 메세지 여부 표시
    socket.emit("check newPrivate", selectedNewPrivate);
    socket.to(userID).emit("check newPrivate", selectedNewPrivate);
  });

  socket.on("public message", async ({ content, to, username }) => {
    // Public Room 글 마지막 인덱스 번호
    let lastIndex = await messageStore.getListSize('all user');
    let newMessage = [];
    
    let allSessionID = await sessionStore.findAllSessionID(); // 모든 세션ID
    await new Promise(resolve => {
      for (let i = 0; i < allSessionID.length; i++) {
        const sessionID = allSessionID[i].replace("session:", ""); // 세션ID

        // 세션정보 사용
        sessionStore.findSession(sessionID).then(session => {
          // Public Room 메세지를 전부 읽었고, Public Room을 보고있는 상태가 아닌 경우
          if (session.newPublic === "false" && session.watching !== "all user") {
            // Public Room 마지막 인덱스 번호를 포함해서 세션정보 업데이트
            sessionStore.saveSession(sessionID, {
              userID    : session.userID,
              username  : session.username,
              password  : session.password,
              connected : session.connected,
              newPrivate: session.newPrivate,
              newPublic : lastIndex,
              watching  : session.watching,
            });
          }
          
          switch (session.watching) {
            case "all user":
              // Public Room을 보고있는 경우, 새 메세지 표시하지 않음
              newMessage.push({userID:session.userID,newMessage:false});
              break;
            default:
              // 보고있지 않은 경우, 새 메세지 표시
              newMessage.push({userID:session.userID,newMessage:true});
              break;
          }
        });

        // for문이 종료되면 Promise 반환 후 await 종료
        if ((allSessionID.length - 1) === i) resolve();
      }
    });

    // 모든 세션의 새 메세지(newPublic) 인덱스 값
    let unread = await sessionStore.findSessionIDByNewPublicValues();
    socket.emit("check newPublic", unread, newMessage);
    socket.broadcast.emit("check newPublic", unread, newMessage);

    // 저장할 메세지
    const message = {
      content,
      from: socket.userID,
      to,
      username
    };

    // 다른 유저에게 메세지 표시
    socket.broadcast.emit("public message", message);
    // 메세지 저장
    messageStore.saveMessage(message);
  });

  socket.on("check newPublic", async userID => {
    const sessionID = await sessionStore.findSessionIDByUserID(userID); // 세션ID
    const session   = await sessionStore.findSession(sessionID);        // 세션정보

    // Public Room 읽음 표시 적용 후 세션정보 업데이트
    sessionStore.saveSession(sessionID, {
      userID    : session.userID,
      username  : session.username,
      password  : session.password,
      connected : session.connected,
      newPrivate: session.newPrivate,
      newPublic : false,
      watching  : session.watching,
    });

    // 모든 세션의 새 메세지(newPublic) 인덱스 값
    const unread = await sessionStore.findSessionIDByNewPublicValues();
    socket.emit("check newPublic", unread);
    socket.broadcast.emit("check newPublic", unread);
  });

  // 현재 보고있는 유저ID(메시지 LIST 세션값) 기록
  socket.on("watching session", async (userID) => {
    const sessionID = await sessionStore.findSessionIDByUserID(socket.userID); // 세션ID
    const session   = await sessionStore.findSession(sessionID);               // 세션정보

    // watching 값 적용 후 세션정보 업데이트
    sessionStore.saveSession(sessionID, {
      userID    : session.userID,
      username  : session.username,
      password  : session.password,
      connected : session.connected,
      newPrivate: session.newPrivate,
      newPublic : session.newPublic,
      watching  : userID,
    });
  });

  // 세션 연결 종료
  socket.on("disconnect", async () => {
    const matchingSockets = await io.in(socket.userID).allSockets();
    const isDisconnected  = matchingSockets.size === 0;

    if (isDisconnected) {
      // 현재 유저 로그아웃, 다른 유저에게 공지
      socket.broadcast.emit("user disconnected", socket.userID);

      // 연결상태 수정
      const session = await sessionStore.findSession(socket.sessionID);
      if (session) {
        sessionStore.saveSession(socket.sessionID, {
          userID    : socket.userID,
          username  : socket.username,
          password  : socket.password,
          connected : false,
          newPrivate: session.newPrivate,
          newPublic : session.newPublic,
          watching  : false,
        });
      }
    }
  });
});

setupWorker(io);
