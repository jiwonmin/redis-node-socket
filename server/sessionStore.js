// 세션 유지 시간 (초)
const SESSION_TTL = (24 * 60 * 60) * 8; // 8일
const mapSession = ([userID, username, password, connected, newPrivate, newPublic, watching]) =>
  userID ? { userID, username, password, connected: connected === "true", newPrivate, newPublic, watching } : undefined;

class RedisSessionStore {
  constructor(redisClient) {
    this.redisClient = redisClient;
  }

  // 세션ID로 세션정보 찾기
  findSession(id) {
    return this.redisClient
    .hmget(`session:${id}`, "userID", "username", "password", "connected", "newPrivate", "newPublic", "watching")
    .then(mapSession);
  }

  // 세션정보 저장
  saveSession(id, { userID, username, password, connected, newPrivate, newPublic, watching }) {
    this.redisClient
    .multi()
    .hset(
      `session:${id}`,
      "userID",
      userID,
      "username",
      username,
      "password",
      password,
      "connected",
      connected,
      "newPrivate",
      newPrivate,
      "newPublic",
      newPublic,
      "watching",
      watching
    )
    .expire(`session:${id}`, SESSION_TTL)
    .exec();
  }

  // 모든 세션정보 반환
  async findAllSessions() {
    const keys = new Set();
    let nextIndex = 0;
    do {
      const [nextIndexAsStr, results] = await this.redisClient.scan(
        nextIndex,
        "MATCH",
        "session:*",
        "COUNT",
        "100" // 100개까지 검색
      );
      nextIndex = parseInt(nextIndexAsStr, 10);
      results.forEach((s) => keys.add(s));
    } while (nextIndex !== 0);
    const commands = [];
    keys.forEach((key) => {
      commands.push(["hmget", key, "userID", "username", "password", "connected", "newPrivate", "newPublic", "watching"]);
    });
    return this.redisClient
      .multi(commands)
      .exec()
      .then((results) => {
        return results
          .map(([err, session]) => (err ? undefined : mapSession(session)))
          .filter((v) => !!v);
      });
  }

  // 유저ID로 검색 후 세션 제거
  async removeSession(userID) {
    const keys = await this.redisClient.keys("session:*");

    let result = false;
    for (const key of keys) {
      const hget = await this.redisClient.hget(key, "userID");
      if (hget === userID) {
        result = true;
        this.redisClient.del(key);
        break;
      }
    }

    return result;
  }

  // 입력한 username, password 값으로 유저 찾기
  async findUserByLoginInfo(inputUsername, inputCryptoPassword) {
    const keys = await this.redisClient.keys("session:*");

    let result = false;
    for (const key of keys) {
      const username = await this.redisClient.hget(key, "username");
      const password = await this.redisClient.hget(key, "password");
      if (username === inputUsername && password === inputCryptoPassword) {
        result = true;
        break;
      }
    }

    return result;
  }

  // 입력한 username 값으로 유저 찾기
  async findUsernameByInputName(inputUsername) {
    const keys = await this.redisClient.keys("session:*");

    let result = false;
    for (const key of keys) {
      const username = await this.redisClient.hget(key, "username");
      if (username === inputUsername) {
        result = true;
        break;
      }
    }

    return result;
  }

  // 입력한 username 값으로 세션ID 찾기
  async findSessionIDByUsername(inputUsername) {
    const keys = await this.redisClient.keys("session:*");

    let result = false;
    for (const key of keys) {
      const username = await this.redisClient.hget(key, "username");
      if (username === inputUsername) {
        result = key;
        break;
      }
    }

    return result;
  }

  // 입력한 username 값으로 유저ID 찾기
  async findUserIDByUsername(inputUsername) {
    const keys = await this.redisClient.keys("session:*");

    let result = false;
    for (const key of keys) {
      const username = await this.redisClient.hget(key, "username");
      const userID = await this.redisClient.hget(key, "userID");
      if (username === inputUsername) {
        result = userID;
        break;
      }
    }

    return result;
  }

  // 유저ID 값으로 세션ID 찾기
  async findSessionIDByUserID(userID) {
    const keys = await this.redisClient.keys("session:*");

    let result = '';
    for (const key of keys) {
      const hget = await this.redisClient.hget(key, "userID");
      if (hget === userID) {
        result = key;
        break;
      }
    }

    return result.replace('session:', '');
  }

  // 모든 세션의 newPublic 값 반환
  async findSessionIDByNewPublicValues() {
    const keys = await this.redisClient.keys("session:*");
    
    let result = [];
    for (const key of keys) {
      const hget = await this.redisClient.hget(key, "newPublic");
      if (hget !== "false") {
        result.push(hget);
      }
    }

    return result;
  }

  async findAllSessionID() {
    return await this.redisClient.keys("session:*");
  }
}

module.exports = {
  RedisSessionStore
};
