const cluster       = require("cluster");
const http          = require("http");
const {setupMaster} = require("@socket.io/sticky");

// 로드밸런싱 워커(프로세서) 개수
const WORKERS_COUNT = 4;

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  for (let i = 0; i < WORKERS_COUNT; i++) {
    cluster.fork();
  }

  cluster.on("exit", (worker) => {
    console.log(`Worker ${worker.process.pid} died`);
    cluster.fork();
  });

  const httpServer = http.createServer();
  setupMaster(httpServer, {
    // 로드밸런싱 방식 (random, round-robin, least-connection)
    loadBalancingMethod: "least-connection",
  });
  const PORT = process.env.PORT || 8000;

  httpServer.listen(PORT, () =>
    console.log(`server listening at port : ${PORT}`)
  );
} else {
  console.log(`Worker ${process.pid} started`);
  require("./index");
}
